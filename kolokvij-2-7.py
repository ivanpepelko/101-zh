class TekuciRacun:
	def __init__(self, ime, prezime, iznosNaRacunu, dozvoljeniMinus=0):
		self.ime = ime
		self.prezime = prezime
		self.iznosNaRacunu = iznosNaRacunu
		self.dozvoljeniMinus = dozvoljeniMinus

	def Uplata(self, iznos):
		self.iznosNaRacunu += iznos

	def Isplata(self, iznos):
		if iznos > self.iznosNaRacunu + self.dozvoljeniMinus:
			print('Nazalost, isplata nije moguca')
			return

		self.iznosNaRacunu -= iznos

	def Stanje(self):
		return self.iznosNaRacunu

	def PromjenaDozvoljenogIznosa(self):
		self.dozvoljeniMinus += 100


mojRacun = TekuciRacun('Ivan', 'Pepelko', 1000)
mojRacun.Isplata(700)
for n in range(5):
	mojRacun.PromjenaDozvoljenogIznosa()

mojRacun.Uplata(4000)
mojRacun.Isplata(5000)

print('Stanje racuna: %d' % mojRacun.Stanje())
