# 6.Napišite program koji učitava prirodan broj n, a zatim n ocjena. Program treba ispisivati prolaznu ocjenu. Ako je barem jedna negativna ocjena,
# prolazna je ocjena nedovoljan, a inače je aritmetička sredina zaokružena na najbliži prirodan broj.

# tekst zadatka je nejasan, ako se unese ocjena 1, petlja se moze prekinuti i ranije, te samo ispisati da ocjena nije prolazna

numInputs = int(input('Broj unosa:'))

isNegative = False
sum = 0
i = 0
while i < numInputs:
	score = int(input('Ocjena:'))
	if score == 1:
		isNegative = True
	else:
		sum += score
	i += 1

if isNegative:
	print('Ocjena nije prolazna')
else:
	# https://docs.python.org/3/library/functions.html#round
	print('Prosjek ocjena je %d' % round(sum / numInputs))
