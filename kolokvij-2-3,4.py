# Napišite program u kojem će korisnik unijeti prirodan broj N te će zatim unijeti sljedeće
# podatke za N učenika: Ime, prezime, ocjenu iz matematike, ocjenu iz informatike i završni
# uspjeh. Stvorite tekstualnu datoteku „Studenti.txt“ te u nju zapišite podatke o učenicima.

nStudenata = int(input('Unesite broj ucenika: '))

studenti = []

for i in range(nStudenata):
	student = [input('Ime: '), input('Prezime: '), input('Ocjena MAT: '), input('Ocjena INF: '), input('Zavr. ocjena: ')]
	studenti.append(student)

studentitxt = open('Studenti.txt', 'w')

for student in studenti:
	for vr in student:
		studentitxt.write('%s\t' % vr)
	studentitxt.write('\n')

studentitxt.close()

# 4. Napišite program koji otvara tekstualnu datoteku „Studenti.txt“ za čitanje te izračunava i ispisuje sljedeće:
# i. Prosječnu ocjenu iz matematike
# ii. Broj studenata koji imaju veću ocjenu iz informatike od prosječne ocjene iz matematike
# iii. Ime i prezime studenta s najboljim završnim uspjehom

studentitxt = open('Studenti.txt', 'r')
# podatke iz datoteke spremamo u listu
podaci = []

for red in studentitxt:
	# kod citanja pojedinog reda razbijamo red sa '\t' kako bi podaci vec bili pripremljeni kod obrade
	podaci.append(red.split('\t'))

studentitxt.close()
# podaci su sada dvodimenzionalna lista

# prvo racunamo sumu svih ocjena iz matematike
sumaOcjenaMat = 0
for student in podaci:
	sumaOcjenaMat += int(student[2])
# za prosjek koristimo duzinu liste podaci (ukupan broj studenata)
prosjekMat = sumaOcjenaMat / len(podaci)

# varijabla-brojac za 2. dio zadatka
iznadProsjeka = 0
for student in podaci:
	# pristupamo ocjeni iz informatike, pretvaramo u float i usporedujemo sa prosjecnom ocjenom iz mat
	if float(student[3]) > prosjekMat:
		iznadProsjeka += 1  # ako je ocjena iznad prosjeka povecavamo brojac

najboljiStudent = podaci[0]
for student in podaci:
	if int(student[4]) > int(najboljiStudent[4]):
		najboljiStudent = student

print('Prosjecna ocjena MAT: %f' % prosjekMat)
print('Broj studenata koji imaju veću ocjenu iz informatike od prosječne ocjene iz matematike: %d' % iznadProsjeka)
print('Najbolju zavrsnu ocjenu ima: %s %s' % (najboljiStudent[0], najboljiStudent[1]))
