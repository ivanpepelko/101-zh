# 5.Napišite program koji unosi prva dva člana aritmetičkog niza te prirodan broj n. Program treba ispisati prvih n elemanata tog niza.

num1 = int(input('Prvi broj niza:'))
num2 = int(input('Drugi broj niza:'))
n = int(input('Broj clanova za ispis:'))

# razlika izmedu dva clana
diff = num2 - num1

# ponavljamo n puta (dok je n > 0, n smanjujemo na kraju petlje)
while n > 0:
	print(num1)  # prvi clan
	num1 += diff  # dodajemo razliku na taj broj
	n -= 1
