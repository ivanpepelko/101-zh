# Napišite program u kojem ćete definirati klasu KvadratnaFunkcija čiji će atributi biti koeficijenti kvadratne funkcije f (x)  ax2  bx  c.
#  Klasa ima sljedeće četiri metode:
# i. FunkcijskaVrijednost – prima jedan argument x tipa float te računa i vraća f(x)
# ii. Diskriminanta - računa i vraća diskriminantu
# iii. Nultocke - računa nultočke i vraća ih ako su one realne, a u protivnom vraća string „Kompleksni brojevi“
# iv. Tjeme - računa i vraća koordinate tjemena
#
# U glavnom programu definirajte kvadratne funkcije f (x)  2 x 2  9 x  10 i g(x)   3x 2  5x  8 .
# Izračunajte i ispišite njihove diskriminante, nultočke i koordinate tjemena. Zatim ispišite
# koliko iznosi f(-5) i g(70). Na kraju, provjerite je li aritmetička sredina nultočaka funkcije f
# jednaka x koordinati tjemena.


from math import sqrt  # import sqrt funkcije (korijen)


class KvadratnaFunkcija:
	def __init__(self, a: float, b: float, c: float):  # inicijalizacija atributa (koeficijenti kvadratne funkcije)
		self.a = a
		self.b = b
		self.c = c

	def FunkcijskaVrijednost(self, x: float):  # f(x) = ax^2 + bx + c
		return self.a ** 2 * x + self.b * x + self.c

	def Diskriminanta(self):  # d = b^2 - 4ac
		return self.b ** 2 - 4 * self.a * self.c

	def Nultocke(self):
		d = self.Diskriminanta()

		# ako je d < 0, nultocke su kompleksni brojevi
		if d < 0:
			return "kompleksni brojevi"

		# racunanje vrijednosti x1, x2
		x1 = -self.b + sqrt(d) / (2 * self.a)
		x2 = -self.b - sqrt(d) / (2 * self.a)

		return x1, x2

	def Tjeme(self):  # x = -b / 2a; y = -d / 4a
		x = -self.b / (2 * self.a)
		y = -self.Diskriminanta() / (4 * self.a)

		return x, y


fx = KvadratnaFunkcija(2, 9, 10)
gx = KvadratnaFunkcija(-3, 5, -8)

print('diskriminanta f(x): %f' % fx.Diskriminanta())
fx0 = fx.Nultocke()
if type(fx0) is str:
	print('Nema nultočke, %s' % fx0)
else:
	print('nultočke f(x): [%f, 0] [%f, 0]' % fx.Nultocke())
print('tjeme f(x): [%f, %f]' % fx.Tjeme())

print()

print('diskriminanta g(x): %f' % gx.Diskriminanta())
gx0 = gx.Nultocke()
if type(gx0) is str:
	print('Nema nultočke, %s' % gx0)
else:
	print('nultočke g(x): [%f, 0] [%f, 0]' % gx.Nultocke())
print('tjeme g(x): [%f, %f]' % gx.Tjeme())

print()

print('f(-5) = %f' % fx.FunkcijskaVrijednost(-5))
print('g(70) = %f' % gx.FunkcijskaVrijednost(70))
