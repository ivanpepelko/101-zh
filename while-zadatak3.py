# 3.Kolika je suma svih prirodnih brojeva manjih od 10000 koji su višekratnici brojeva 3 ili 4.

i = 0
sum = 0

while i < 10000:
	if i % 3 == 0 or i % 4 == 0:
		sum += i
	i += 1

print(sum)
