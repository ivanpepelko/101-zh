# 2. Napišite skriptu koja od korisnika traži unos prirodnog broja N te zatim korisnik unosi N realnih brojeva koji se spremaju u listu naziva mojaLista.
#
# - Stvorite i ispišite listu naziva generickaLista koja ima ukupno N visekratnika broja 6 pocevsi od broja 42
# - Stvorite i ispisite listu naziva novaLista koja takoder ima N elemenata pri cemu ce elementi na neparnim indexima biti nule, a na parnim indexima ce biti
# aritmeticke sredine brojeva iz listi mojaLista i generickaLista na tim istim indexima.
# - Krenuvsi od pocetka, odredite i ispisite prvi element liste mojaLista koji je veci od 10. Ukoliko takav ne postoji, ispisite poruku "Ne postoji"

n = int(input("Unesite broj elemenata:"))

mojaLista = []
for i in range(n):
	mojaLista.append(float(input("Broj:")))
print(mojaLista)

# genericka lista koristenjem for petlje
# generickaLista = []
# for i in range(n):
#     generickaLista.append(42 + i * 6)

# drugi nacin
# generickaLista = []
# for i in range(42, 42 + n * 6, 6):
#     generickaLista.append(i)

# pretvaranjem outputa range funkcije; range sam po sebi ne vraca listu, potrebno ga je pretvoriti sa list()
generickaLista = list(range(42, 42 + n * 6, 6))
print(generickaLista)

novaLista = []
for i in range(n):
	if i % 2 == 0:
		# aritmeticka sredina brojeva iz 'mojaLista' i 'generickaLista' na indexu [i]
		novaLista.append((mojaLista[i] + generickaLista[i]) / 2)
	else:
		novaLista.append(0)
print(novaLista)

# ovu vrijednost koristiti cemo ako ne pronademo broj veci od 10
hasMemberLargerThan10 = False
for num in mojaLista:
	# ako je broj veci od 10 pronaden, ispisujemo ga odmah i prekidamo petlju
	if num > 10:
		print(num)
		# takoder postavljamo ovu vrijednost na True kako se kasnije if nebi izvrsio
		hasMemberLargerThan10 = True
		break

if not hasMemberLargerThan10:
	print('Ne postoji')
