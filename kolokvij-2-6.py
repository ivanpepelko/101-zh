def spol_osobe(ime):
	if ime[-1] == 'a' or ime[-1] == 'A':
		return 0
	return 1


def ocjene(ocjena1, ocjena2):
	if 1 <= ocjena1 <= 5 and 1 <= ocjena2 <= 5:
		return ocjena1, ocjena2
	return 0, 0


n = int(input("unesi broj:"))
f = open("anketa.txt", "w")
for i in range(n):
	ime = input("ime:")
	prezime = input("prezime:")
	ocjena1 = int(input("ocjena prvog proizvoda:"))
	ocjena2 = int(input("ocjena drugog proizvoda:"))
	(novaOcjena1, novaOcjena2) = ocjene(ocjena1, ocjena2)
	if spol_osobe(ime) == 0:
		f.write("ženska osoba " + ime + " " + prezime + " je dala ocjene " + str(novaOcjena1) + " i " + str(novaOcjena2) + "\n")
	else:
		f.write("muška osoba " + ime + " " + prezime + " je dala ocjene " + str(novaOcjena1) + " i " + str(novaOcjena2) + "\n")

f.close()
