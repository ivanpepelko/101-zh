# 4.Ispišite sve Fibonaccijeve brojeve manje od 100.

# fibonaccijev niz pocinje s [0,1] ili [1,1]
current = 0
prev = 1

# ispituje se prijasnji broj da bi petlja mogla prekinuti ako je veci od 100
while prev < 100:
	print(prev)  # prvo ispisemo prvi broj niza
	fib = current + prev  # izracunamo slijedeci

	# pomicemo niz na slijedece 2 vrijednosti
	current = prev
	prev = fib
