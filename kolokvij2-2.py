# Definirajte funkciju koja umeće zadani znak ispred svakog samoglasnika u zadanoj
# rečenici. Funkcija ima dva argumenta tipa string koji označavaju rečenicu i znak te vraća
# promijenjenu rečenicu. U glavnom programu pitajte korisnika da unese rečenicu i znak te
# pozovite funkciju s tim parametrima.


def dodajZnakIS(recenica: str, znak: str):
	novaR = ''  # init prazni string

	for z in recenica:  # prolazimo kroz svaki znak u recenici
		# provjeravamo jeli znak samoglasnik, koristimo .lower() kak nebi trebali provjeravati i velika slova
		if z.lower() in ['a', 'e', 'i', 'o', 'u']:
			novaR += znak  # ako je trenutni znak samoglasnik ispred njega umecemo dodatni znak

		novaR += z  # uvijek dodajemo trenutni znak for petlje

	return novaR


recenica = input('Unesite recenicu: ')
znak = input('Unesite znak ispred samoglasnika: ')

print(dodajZnakIS(recenica, znak))
